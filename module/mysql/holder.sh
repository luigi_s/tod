#!/bin/bash
#
# holder.sh - mysql
#
# Copyright 2019 Luigi Santivetti <luigi.santivetti@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Placeholders must follow a special syntax in order to be considered valid.
# If they don't, then validation will fail and the process exits with error.
# These restrictions are described below, so that a holders.sh is considered
# valid as described below.

_MYSQL_DKRC_SERVICE_="${module}"
_MYSQL_DKRC_VERSION_="5.7.26"
_MYSQL_DKRC_IMAGE_="${host_name}-img-${_MYSQL_DKRC_SERVICE_}"
_MYSQL_DKRC_CONTAINER_="${host_name}-con-${_MYSQL_DKRC_SERVICE_}"
_MYSQL_DKRC_BACKEND_IP_="172.27.0.2"
_MYSQL_DKRC_CONTEXT_="$mod_docker_d"
_MYSQL_DKRC_DOCKERFILE_="$dockerfile"
_MYSQL_DB_FIELD_USERNAME_="username"
_MYSQL_DB_FIELD_PASSWORD_="password"
_MYSQL_DB_FIELD_KEY_="key"
_MYSQL_DB_FIELD_EXPIRY_="expiry"
_MYSQL_DB_FIELD_VALUE_="value"
_MYSQL_DB_TABLE_SESSIONS_="sessions"
_MYSQL_DB_TABLE_USERS_="users"
_MYSQL_DB_NAME_="${host_name}_db"
_MYSQL_DB_PORT_="3306"
_MYSQL_INITDB_F_="$initdb_f"
_MYSQL_MYCONF_F_="$mycnf_f"
_MYSQL_MYSQL_D_="$mysql_d"
_MYSQL_LOG_D_="$log_d"
_MYSQL_BACKUP_PATH_="/var/lib/mysql"
if [ "${mod_mode}" = "${release}" ]; then
	_MYSQL_GENERAL_LOG_="0"
	_MYSQL_SLOW_QUERY_LOG_="0"
	_MYSQL_LOG_OUTPUT_="NONE"
else
	_MYSQL_GENERAL_LOG_="1"
	_MYSQL_SLOW_QUERY_LOG_="1"
	_MYSQL_LOG_OUTPUT_="FILE"
fi
_MYSQL_CON_LOG_D_="/var/log/mysql"
_MYSQL_CON_ERROR_LOG_F_="${_MYSQL_CON_LOG_D_}/$(basename "$log_error_f")"
_MYSQL_CON_GENERAL_LOG_F_="${_MYSQL_CON_LOG_D_}/$(basename "$log_general_f")"
_MYSQL_CON_SLOW_QUERY_LOG_F_="${_MYSQL_CON_LOG_D_}/$(basename "$log_query_f")"
if [ "$mysql_has_dummy_certs" -eq 1 ]; then
	_MYSQL_ROOTFS_SSL_D_="$certificates_l"
	_MYSQL_CON_SSL_D_="/etc/mysql_ssl"

	_MYSQL_SSL_SERVER_CERT_F_="${_MYSQL_CON_SSL_D_}/$(basename "$server_cert_f")"
	_MYSQL_SSL_SERVER_KEY_F_="${_MYSQL_CON_SSL_D_}/$(basename "$server_key_f")"
	_MYSQL_SSL_SERVER_CA_F_="${_MYSQL_CON_SSL_D_}/$(basename "$ca_f")"

	_MYSQL_SSL_CLIENT_CERT_F_="${_MYSQL_CON_SSL_D_}/$(basename "$client_cert_f")"
	_MYSQL_SSL_CLIENT_KEY_F_="${_MYSQL_CON_SSL_D_}/$(basename "$client_key_f")"
	_MYSQL_SSL_CLIENT_CA_F_="${_MYSQL_CON_SSL_D_}/$(basename "$ca_dup_f")"
fi
