#!/bin/bash
#
# module.sh - test2
#
# Copyright 2019 Luigi Santivetti <luigi.santivetti@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

declare -r module="test2"
source $common_sh

# Test dependencies. Comment will cause validation to detect that test1 has one
# unmet dependency on test2.
module_enable $module

declare -r tmp_d="${mod_rootfs_d}/tmp"
declare -r test="${module}-testfile.txt"
declare -r test_f="$tmp_d/$test"

# Test name clash. test1 and test2 both declare a global variable mod_name_clash
declare -r mod_name_clash="test2_var1_module"

declare -ar mod_more_dirs=( $tmp_d )
declare -ar mod_more_files=( $test_f )
declare -ar mod_more_trefs=( test_t )

function tod_watch
{
	__watch_module_common || return $s_err
}

function tod_doins
{
	__doins_module_common || return $s_err
}

function tod_upins
{
	__upins_module_common || return $s_err
}

function tod_clins
{
	__clins_module_common || return $s_err
}
