#!/bin/bash
#
# holder.sh
#
# Copyright 2019 Luigi Santivetti <luigi.santivetti@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# gerrit template
_GERRIT_DKRC_SERVICE_="${module}"
_GERRIT_DKRC_IMAGE_="${host_name}-img-${_GERRIT_DKRC_SERVICE_}"
_GERRIT_DKRC_CONTAINER_="${host_name}-con-${_GERRIT_DKRC_SERVICE_}"
_GERRIT_DKRC_FRONTEND_IP_="172.28.0.3"
_GERRIT_DKRC_DOCKERFILE_="${dockerfile}"
_GERRIT_DKRC_ENTRYPOINT_="${entrypoint}"
_GERRIT_DKRC_CONTEXT_="${mod_docker_d}"
_GERRIT_DKRC_ROOTFS_="${mod_rootfs_d}"
_GERRIT_DKRC_ETC_D_="${etc_d}"
if [ "$gerrit_has_https" -eq 1 ]; then
	_GERRIT_HAS_HTTPS_="1"
	_GERRIT_DKRC_KEYSTORE_F_="${keystore_f}"
	_GERRIT_DKRC_SSLIB_F_="${jar_sslib_f}"
	_GERRIT_KEYSTORE_="/var/gerrit/etc/$(basename "$keystore_f")"
	_GERRIT_SSLIB_="/var/gerrit/lib/${jar_sslib}.jar"
	_GERRIT_SECURE_STORE_="com.googlesource.gerrit.plugins.secureconfig.SecureConfigStore"
	_GERRIT_SECURE_CIPHER_="PBEWithSHA1AndRC2_40"
	_GERRIT_PROXY_PORT_="443"
	_GERRIT_PROXY_PROTOCOL_="https"
	_GERRIT_PASSWD_F_="${PASSWD_F}"
else
	_GERRIT_HAS_HTTPS_="0"
	_GERRIT_DKRC_KEYSTORE_F_=""
	_GERRIT_DKRC_SSLIB_F_=""
	_GERRIT_KEYSTORE_=""
	_GERRIT_SSLIB_=""
	_GERRIT_SECURE_STORE_=""
	_GERRIT_SECURE_CIPHER_=""
	_GERRIT_PROXY_PORT_="8079"
	_GERRIT_PROXY_PROTOCOL_="proxy-http"
fi
_GERRIT_SSH_PORT_="29418"
_GERRIT_CANON_URL_="${_GERRIT_PROXY_PROTOCOL_}://www.${host_name}/gerrit/"
_GERRIT_APT_GET_PACKAGE_="${war_version}-${revision}"
_GERRIT_LISTEN_URL_="${_GERRIT_PROXY_PROTOCOL_}://${_GERRIT_DKRC_FRONTEND_IP_}:${_GERRIT_PROXY_PORT_}/gerrit/"
_GERRIT_JAVA_URANDOM_="-Djava.security.egd=file:/dev/./urandom"
_GERRIT_JAVA_HOME_D_="/usr/lib/jvm/java-1.8.0-openjdk-amd64/jre"
if [ "${mod_mode}" = "${debug}" ]; then
	_GERRIT_APT_GET_INSTALL_NMAP_="RUN apt-get -y install nmap"
	_GERRIT_APT_GET_INSTALL_PING_="RUN apt-get -y install net-tools"
else
	_GERRIT_APT_GET_INSTALL_NMAP_="# Skip install nmap for ${mod_mode}"
	_GERRIT_APT_GET_INSTALL_PING_="# Skip install ping for ${mod_mode}"
fi
