#!/bin/bash
#
# module.sh - apache
#
# Copyright 2019 Luigi Santivetti <luigi.santivetti@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

declare -r module="apache"
source $common_sh

module_enable $module
declare -ar depmod=( mysql gerrit )

declare -r etc_d="$mod_rootfs_d/etc"
declare -r www_d="$mod_rootfs_d/var/www"
declare -r log_d="$mod_rootfs_d/var/log/apache2"

declare -r application_d="$www_d/application"
declare -r html_d="$www_d/html"
declare -r auth_d="$www_d/auth"

declare -r conf_d="$etc_d/apache2"
declare -r available_d="$conf_d/sites-available"
declare -r enabled_d="$conf_d/sites-enabled"
declare -r views_d="$application_d/views"
declare -r controllers_d="$application_d/controllers"

# Docker context directory
declare -r mod_authnz_external_mirror="mod-auth-external.git"
declare -r mod_authnz_external_branch="mod_authnz_external-3.3.3"
declare -r mod_authnz_external_d="${mod_docker_d}/mod_authnz_external"

# Output files
declare -r ports_f="$conf_d/ports.conf"
declare -r apache2_f="$conf_d/apache2.conf"
declare -r dockerfile_f="$mod_docker_d/Dockerfile"
declare -r vhost_http_f="$available_d/vhost-http.conf"
declare -r vhost_https_f="$available_d/vhost-https.conf"
declare -r vhost_gerrit_f="$available_d/vhost-gerrit.conf"
declare -r http_authentication_bang_f="${www_d}/http_authentication"

# Staged resources
declare -r backend_d="$mod_staging_d/code-igniter-v3-giggi.git"
declare -r frontend_d="$mod_staging_d/pelican-subtle-giggi.git"
declare -r content_d="$mod_staging_d/mdcon.git"
declare -r pythonenv_d="$mod_staging_d/pelican-venv"

declare -r pelican_version="4.0.1"
declare -r markdown_version="3.1.1"
declare -r setuptools_version="45"

declare -ir apache_has_dummy_certs=1
if [ $apache_has_dummy_certs -eq 1 ]; then
	declare -r letsencrypt_d="$mod_staging_d/dummy_cert"
else
	declare -r letsencrypt_d="/etc/letsencrypt"
fi
declare -r certificates_l="$etc_d/certificates"

declare -ar PELICAN_CLEAN_TARGETS=( pelican-all )
declare -r apache_has_media=0
if [ $apache_has_media -eq 1 ]; then
	declare -r clean_skiptag_sh="clean_skiptag.sh"
	declare -r skiptag="_.${host_name}."
	declare -r media_d="$HOME/media"
	declare -r photos_d="$media_d/photos"
	declare -r videos_d="$media_d/videos"
	declare -ar PELICAN_MAKE_TARGETS=( pelican-all )
	declare -ar PELICAN_INSTALL_TARGETS=( pelican-all )
else
	declare -ar PELICAN_MAKE_TARGETS=( pelican-html )
	declare -ar PELICAN_INSTALL_TARGETS=( pelican-html pelican-theme )
fi
declare -ar CODEIGN_INSTALL_TARGETS=( codeign-all )

declare -ar mod_more_dirs=( \
	$www_d $html_d $auth_d $etc_d $conf_d $log_d $available_d $enabled_d \
		   $application_d $views_d $controllers_d $mod_authnz_external_d \
)

declare -ar mod_more_files=( \
	$ports_f $apache2_f $vhost_http_f $vhost_https_f $vhost_gerrit_f \
	$dockerfile_f $http_authentication_bang_f \
)

declare -ar mod_more_trefs=( \
	ports_t apache2_t vhost_http_t vhost_https_t vhost_gerrit_t \
	dockerfile_t http_authentication_bang_t \
)

function tod_watch
{
	__watch_module_common || return $s_err
}

function tod_doins
{
	__doins_module_common || return $s_err
	__apache_do_sites     || return $s_err
	__apache_do_links     || return $s_err
}

function tod_fetch
{
	__fetch_module_common || return $s_err
}

function tod_upins
{
	__upins_module_common || return $s_err
}

function tod_upmod
{
	__fetch_module_common   || return $s_err
	__apache_do_ext_modules || return $s_err
	__apache_do_dummy_cert  || return $s_err
	__apache_do_backend     || return $s_err
	__apache_do_frontend    || return $s_err
}

function tod_clmod
{
	__clmod_module_common || return $s_err
}

function tod_clins
{
	__clins_module_common || return $s_err
}

function tod_doall
{
	# upmod must come after
	tod_doins && tod_upmod || return $s_err
}

function tod_upall
{
	# upmod must come after
	tod_upins && tod_upmod || return $s_err
}

function __apache_do_ext_modules
{
	local origin_d="${mod_staging_d}/${mod_authnz_external_mirror}"
	rm -rf ${mod_authnz_external_d}/* || return $s_err
	cp -ar ${origin_d}/* ${mod_authnz_external_d}
}

function __apache_do_dummy_cert_helper
{
	local -r digital_sign="digitalSignature\nextendedKeyUsage=serverAuth"
	local -r dn_dns="DNS:${host_name}\nkeyUsage=$digital_sign"
	local -r dn_lhs="[dn]\nCN=${host_name}\n[req]\ndistinguished_name"
	local -r dn_rhs="dn\n[EXT]\nsubjectAltName"

	openssl req -x509 -out ${host_name}.crt -keyout ${host_name}.key \
			-newkey rsa:2048 -nodes -sha256 \
			-subj "/CN=${host_name}" -extensions EXT \
			-config <(printf "$dn_lhs = $dn_rhs=$dn_dns") 2>&1 | lets -l -x "openssl"

	return ${PIPESTATUS[0]}
}

function __apache_do_dummy_cert
{
	if [ $apache_has_dummy_certs -eq 1 ]; then
		if [ ! -d $letsencrypt_d ]; then
			mkdir -p $letsencrypt_d && \
				pushd $letsencrypt_d >/dev/null || return $s_err
			__apache_do_dummy_cert_helper && \
				echo "# Dummy $host_name conf" > $host_name.conf && \
				popd >/dev/null || {
					lets -l -e "failed to make dummy certificates"
					popd >/dev/null; return $s_err
				}
		fi
	fi

	lets -l -i "using $module SSL certificates from: $letsencrypt_d"
	return $s_ok
}

function __apache_do_links
{
	__apache_do_dummy_cert || return $s_err

	# NOTE: container path must match localhost path
	#
	# i.e.
	#    localhost:/etc container:/etc                 OK
	#    localhost:/home/luigi container:/home/???     NOT OK
	#
	ln -sf "${letsencrypt_d}" "${certificates_l}"
}

function __apache_do_sites
{
	if [ ! -d "$enabled_d" ]; then
		lets -l -e "invalid: $enabled_d"
		return 1
	fi

	# This list controls what site to enable
	local -a sites=( $vhost_http_f $vhost_https_f $vhost_gerrit_f )

	pushd $enabled_d &>/dev/null

	local file
	for file in ${sites[@]}; do
		local link_name="$(basename "$file")"
		file="../sites-available/$link_name"

		if [ -f "$file" ]; then
			ln -sf $file $link_name
		else
			lets -l -e "failed link $file"
			popd &>/dev/null
			return 1
		fi
	done

	popd &>/dev/null
}

# @1      : absolute path to the target directory
# @2      : (optional) requirements txt file
function __apache_do_venv
{
	local -ar deps=( python3-dev python3-pip python3-venv python3-wheel )

	if [ -z "$1" ] || [ ! -f "$2" ]; then
		lets -l -e "virtual environment: invalid params"
		return $s_err
	fi

	if [ ! -e "$1/bin/activate" ]; then
		local install="python3 -m pip install --no-cache-dir --upgrade"
		local -a global_packages=( "virtualenv" "pip" )
		local -a local_packages=(
			"wheel"
			"setuptools==$setuptools_version"
			"pelican==$pelican_version"
			"markdown==$markdown_version"
		)

		util_install_dependency ${deps[@]} || {
			lets -l -e "install python dependencies"
			return $s_err
		}

		mkdir -p "$1" || {
			lets -l -e "virtual envirnment: mkdir failed"
			return $s_err
		}

		$install --user ${global_packages[@]} 2>&1 | lets -l -x "pip3"

		python3 -m venv "$1" 2>&1 | lets -l -x "python"
		[ "${PIPESTATUS[0]}" -eq 0 ] || {
			lets -l -e "virtual environment: create failed"
			return $s_err
		}

		source "$1/bin/activate" || return $s_err
		(
			$install ${local_packages[@]} 2>&1 | lets -l -x "pip3"
			[ "${PIPESTATUS[0]}" -eq 0 ] || {
				deactivate
				return $s_err2
			}

			while read -r pkg; do
				$install $pkg 2>&1 | lets -l -x "pip3"
				[ "${PIPESTATUS[0]}" -eq 0 ] || {
					deactivate
					return $s_err3
				}
			done < "$2"

			deactivate
		)
		case "$?" in
			$s_err2) lets -l -e "install local packages"; return $s_err ;;
			$s_err3) lets -l -e "install $(basename $2)"; return $s_err ;;
		esac
	else
		lets -l -w "virtual environment: exists already"
	fi

	return $s_ok
}

function __apache_frontend_clean
{
	local target

	for target in ${PELICAN_CLEAN_TARGETS[@]}; do
		case "$target" in
			pelican-html      ) make clean-html   || return $s_err ;;
			pelican-theme     ) make clean-theme  || return $s_err ;;
			pelican-photos    ) make clean-photos || return $s_err ;;
			pelican-videos    ) make clean-videos || return $s_err ;;
			pelican-distclean ) make distclean    || return $s_err ;;
			pelican-all       ) make clean        || return $s_err ;;
		esac
	done
}

function __apache_frontend_make
{
	local target

	for target in ${PELICAN_MAKE_TARGETS[@]}; do
		case "$target" in
			pelican-html)
				PELICAN_PHOTO_LIBRARY=$photos_d \
				PELICAN_PHOTO_EXCLUDEALL="1" \
				PELICAN_VIDEO_LIBRARY=$videos_d \
				PELICAN_VIDEO_EXCLUDEALL="1" \
				PELICAN_SITEURL=$host_name \
				PELICAN_CONTENT=$content_d \
				make html || return $s_err ;;
			pelican-all)
				PELICAN_PHOTO_LIBRARY=$photos_d \
				PELICAN_PHOTO_EXCLUDE=${EXCLUDED_PHOTOS[@]} \
				PELICAN_PHOTO_SKIPTAG=$skiptag \
				PELICAN_VIDEO_LIBRARY=$videos_d \
				PELICAN_VIDEO_EXCLUDE=${EXCLUDED_VIDEOS[@]} \
				PELICAN_SITEURL=$host_name \
				PELICAN_VIDEO_SKIPTAG=$skiptag \
				PELICAN_CONTENT=$content_d \
				PELICAN="pelican" \
				make html || return $s_err ;;
		esac
	done
}


# FIXME: improve integration with skiptag
function __finalise_install
{
	[ ! -f "$1/$clean_skiptag_sh" ] || {
		pushd "$1" &>/dev/null && \
			source "$1/$clean_skiptag_sh" && popd &>/dev/null
	}
}

function __apache_frontend_install
{
	local target

	[ -d "$views_d" ] && [ -d "$html_d" ] || {
		lets -l -w "not installing frontend"
		return $s_ok
	}

	for target in ${PELICAN_INSTALL_TARGETS[@]}; do
		case "$target" in
			pelican-html)
				INSTALLDIR=$views_d \
				make install-html   || return $s_err ;;
			pelican-theme)
				INSTALLDIR_THEME=$html_d \
				make install-theme  || return $s_err ;;
			pelican-photos)
				INSTALLDIR_PHOTOS=$photos_d \
				make install-photos || return $s_err ;;
			pelican-videos)
				INSTALLDIR_VIDEOS=$videos_d \
				make install-videos || return $s_err ;;
			pelican-all)
				INSTALLDIR=$views_d \
				INSTALLDIR_THEME=$html_d \
				INSTALLDIR_PHOTOS=$photos_d \
				INSTALLDIR_VIDEOS=$videos_d \
				make install || return $s_err

				# FIXME: improve integration with skiptag
				__finalise_install $photos_d || \
					lets -l -e "Failed to finalise photos"
				__finalise_install $videos_d || \
					lets -l -e "Failed to finalise videos"
				true ;;
		esac
	done
}

function __create_apache_frontend
{
	if [ $apache_has_media -eq 1 ]; then
		[ -d "$photos_d" ] || mkdir -p "$photos_d" || return $s_err
		[ -d "$videos_d" ] || mkdir -p "$videos_d" || return $s_err
	fi

	set -e; source $1/bin/activate; set +e || return $s_err3

	__apache_frontend_clean   || { deactivate; return $s_err4; }
	__apache_frontend_make    || { deactivate; return $s_err5; }
	__apache_frontend_install || { deactivate; return $s_err6; }
	__apache_frontend_clean   || { deactivate; return $s_err4; }

	deactivate; true
}

function __apache_do_frontend
{
	local req="requirements.txt"
	local file="$(realpath "$(find "$frontend_d" -name $req)")"

	__apache_do_venv $pythonenv_d $file || return 1
	lets -l -d "virtual environment: complete"

	pushd $frontend_d &>/dev/null
	__create_apache_frontend $pythonenv_d 2>&1 | lets -l -x "make"
	case "${PIPESTATUS[0]}" in
		$s_ok   ) popd &>/dev/null; return $s_ok          ;;
		$s_err3 ) lets -l -e "failed to activate python"  ;;
		$s_err4 ) lets -l -e "failed to clean frontend"   ;;
		$s_err5 ) lets -l -e "failed to make frontend"    ;;
		$s_err6 ) lets -l -e "failed to install frontend" ;;
		*       ) lets -l -e "failed unknown error"       ;;
	esac; popd &>/dev/null; return $s_err
}

function __apache_backend_install
{
	local target

	[ -d "$backend_d/application" ] && \
		[ -d "$backend_d/system" ] && [ -f "$backend_d/index.php" ] || {
		lets -l -w "not installing backend"
		return $s_ok
	}

	for target in ${CODEIGN_INSTALL_TARGETS[@]}; do
		case "$target" in
			codeign-all)
				cp -ra $backend_d/application $www_d || return $s_err2
				cp -ra $backend_d/system $www_d      || return $s_err3
				cp -a $backend_d/index.php $html_d   || return $s_err4 ;;
		esac
	done
}

function __create_apache_backend
{
	__apache_backend_install
}

function __apache_do_backend
{
	__create_apache_backend
	case "$?" in
		$s_err2 ) lets -l -e "failed to install application code" ;;
		$s_err3 ) lets -l -e "failed to install system code"      ;;
		$s_err4 ) lets -l -e "failed to install index.html"       ;;
	esac
}
