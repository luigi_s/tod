#!/bin/bash
#
# apache templates
#
# Copyright 2019 Luigi Santivetti <luigi.santivetti@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

declare -r vhost_http_t="\
<VirtualHost *:${_APACHE_HTTP_PORT_}>

	ServerName ${_APACHE_SERVER_NAME_}
	ServerAlias ${_APACHE_SERVER_ALIAS_}

	ErrorLog \"${_APACHE_VHOST_HTTP_LOG_F_}\"
	RewriteEngine on

	# NOTE: any POST or PUT send with http (:80) will be redirected
	# with the side effect of dropping any data sent. We don't care
	# nothing should transit over http anyway.

	# Enforce https and www.
	RewriteCond %{REQUEST_SCHEME} =http [NC]
	RewriteCond %{SERVER_NAME} =${_APACHE_SERVER_NAME_} [NC]
	RewriteRule ^ https://${_APACHE_SERVER_ALIAS_}%{REQUEST_URI} [NE,R=permanent,L]

	# Enforce https
	RewriteCond %{REQUEST_SCHEME} =http [NC]
	RewriteCond %{SERVER_NAME} =${_APACHE_SERVER_ALIAS_} [NC]
	RewriteRule ^ https://${_APACHE_SERVER_ALIAS_}%{REQUEST_URI} [NE,R=permanent,L]

</VirtualHost>"

declare -r vhost_https_t="\
<IfModule mod_ssl.c>
	<VirtualHost *:${_APACHE_HTTPS_PORT_}>

		ServerName ${_APACHE_SERVER_NAME_}
		ServerAlias ${_APACHE_SERVER_ALIAS_}

		# DocumentRoot is the only publicly accessible data:
		#
		#   - index.php, for dispatching requested URIs
		#   - theme/css, style
		#   - theme/js, style

		DocumentRoot \"${_APACHE_DOCUMENT_ROOT_D_}\"
		ErrorLog \"${_APACHE_SSL_LOG_F_}\"
		LogLevel ${_APACHE_SSL_LOG_LEVEL_}

		# Flags:
		#
		#   NE, not escape, as keep chars such as & and ?
		#
		#   R=status, redirect, if a valid URI is generated in the rewrite then
		#   issue a request to the browser. It always prepends the rewrite with
		#   [this protocol]://[thishost][:thisport]
		#
		#   L, last, do not feed the rewrite result to the next rule
		#
		#   NC, non case sensitive
		#
		# NOTE: This is to enforce 'www.' and *DO NOT* rediret! Skip POST and PUT
		# because apache does an internal redirect 301 or 302 and drops the data
		# attached to the request.

		RewriteEngine on
		RewriteCond %{REQUEST_METHOD} !^(POST|PUT) [NC]
		RewriteCond %{SERVER_NAME} =${_APACHE_SERVER_NAME_} [NC]
		RewriteRule ^ https://${_APACHE_SERVER_ALIAS_}%{REQUEST_URI} [NE]

		# Proxy configuration
		AllowEncodedSlashes on

		# Route to vhost-gerrit.conf
		#
		# NOTE: 127.0.0.1 relative to container network

		ProxyPassMatch /gerrit(/?)(.*) http://127.0.0.1:${_APACHE_GERRIT_PROXY_PORT_}/\$2 nocanon
		ProxyPassReverse /gerrit(/?)(.*) http://127.0.0.1:${_APACHE_GERRIT_PROXY_PORT_}/\$2

		# Within this context, it uses a file system path instead of URL path
		#
		#   i.e.
		#   out %{REQUEST_URI} expands to https://servername.domain/whatever/
		#   in  %{REQUEST_URI} expands to var/www/html/whatever/
		#
		# NOTE: %{REQUEST_URI} in directory context expands with a leading slash
		# and a trailing slash

		<Directory \"${_APACHE_DOCUMENT_ROOT_D_}\">

			DirectorySlash Off
			Require all granted

			# This assumes to be calling a php handler, so:
			#
			# NOTE: Do not redirect or it will break the request.

			RewriteCond %{REQUEST_FILENAME} !.*\.(css|js|mp4|jpg)$ [NC]
			RewriteCond %{REQUEST_URI} !^/index.php [NC]
			RewriteRule ^(.*)$ index.php/\$1 [NC,L]

		</Directory>

		# Lock out undesired auth requests

		<Location \"/login/auth\">

			Order Deny,Allow
			Deny from all
			Allow from ${_APACHE_MOD_AUTHNZ_LOCALHOST_}

		</Location>

		<LocationMatch \"^(/invite|/page/invite|/page/invite\\.html)$\">

			AuthType Basic
			AuthName \"Invite @ ${_APACHE_SERVER_NAME_}\"

			# authnz_external
			AuthBasicProvider external

			# Call into php again
			AuthExternal ${_APACHE_EXT_AUTH_KEYWORD_}

			# mod_authz_core configuration
			Require valid-user

		</LocationMatch>

		# NOTE: path relative to container rootfs
		Include ${_APACHE_CON_SSL_CONF_F_}
		SSLCertificateFile ${_APACHE_CON_SSL_CERT_F_}
		SSLCertificateKeyFile ${_APACHE_CON_SSL_KEY_F_}

		# Configure external authentication module
		<IfModule mod_authnz_external.c>
			DefineExternalAuth ${_APACHE_EXT_AUTH_KEYWORD_} pipe ${_APACHE_CON_EXT_AUTH_F_}
		</IfModule>

		# Configure set default environment
		<IfModule mod_env.c>
			SetEnv CI_ENV ${_APACHE_CODE_IGNITER_ENV_}
		</IfModule>

	</VirtualHost>
</IfModule>"

if [ "${_GERRIT_HAS_HTTPS_}" -eq 1 ]; then
	declare -r vhost_gerrit_t_has_ssl="\
	SSLProxyEngine on
	SSLProxyCheckPeerCN off
	SSLProxyCheckPeerName off"
else
	declare -r vhost_gerrit_t_has_ssl=""
fi

declare -r vhost_gerrit_t="\
<VirtualHost 127.0.0.1:${_APACHE_GERRIT_PROXY_PORT_}>

	# Restrict to only requests from 127.0.0.1

	<LocationMatch \".*\">

			Order Deny,Allow
			Deny from all
			Allow from 127.0.0.1

	</LocationMatch>

	ErrorLog \"${_APACHE_VHOST_GERRIT_LOG_F_}\"
	LogLevel ${_APACHE_VHOST_GERRIT_LOG_LEVEL_}

${vhost_gerrit_t_has_ssl}

	ProxyVia off
	ProxyRequests off
	ProxyPreserveHost on
	ProxyErrorOverride on

	AllowEncodedSlashes on
	RewriteEngine on
	# Proxy incoming requests towards Gerrit Code Review
	RewriteRule ^(/?)(.*)$ ${_GERRIT_PROXY_PROTOCOL_}://${_GERRIT_DKRC_FRONTEND_IP_}:${_GERRIT_PROXY_PORT_}/gerrit/\$2 [NE,P]

	<LocationMatch \"(/gerrit/login(/?)|/login(/?))\">

		AuthType Basic
		AuthName \"Gerrit @ ${_APACHE_SERVER_NAME_}\"

		# authnz_external
		AuthBasicProvider external

		# Call into php again
		AuthExternal ${_APACHE_EXT_AUTH_KEYWORD_}

		# mod_authz_core configuration
		Require valid-user

	</LocationMatch>

	# Configure external authentication module
	<IfModule mod_authnz_external.c>
		DefineExternalAuth ${_APACHE_EXT_AUTH_KEYWORD_} pipe ${_APACHE_CON_EXT_AUTH_F_}
	</IfModule>

</VirtualHost>"

declare -r ports_t="\
# If you just change the port or add more ports here, you will likely also
# have to change the VirtualHost statement in
# /etc/apache2/sites-enabled/000-default.conf

#
# Port mapped to the host (only one)
#
Listen ${_APACHE_HTTP_PORT_}

#
# Gerrit, vhost, proxied
#
Listen ${_APACHE_GERRIT_PROXY_PORT_}

<IfModule ssl_module>
	Listen ${_APACHE_HTTPS_PORT_}
</IfModule>

<IfModule mod_gnutls.c>
	Listen ${_APACHE_HTTPS_PORT_}
</IfModule>"

declare -r apache2_t="\
# This is the main Apache server configuration file.  It contains the
# configuration directives that give the server its instructions.
# See http://httpd.apache.org/docs/2.4/ for detailed information about
# the directives and /usr/share/doc/apache2/README.Debian about Debian specific
# hints.
#
#
# Summary of how the Apache 2 configuration works in Debian:
# The Apache 2 web server configuration in Debian is quite different to
# upstream's suggested way to configure the web server. This is because Debian's
# default Apache2 installation attempts to make adding and removing modules,
# virtual hosts, and extra configuration directives as flexible as possible, in
# order to make automating the changes and administering the server as easy as
# possible.

# It is split into several files forming the configuration hierarchy outlined
# below, all located in the /etc/apache2/ directory:
#
#	/etc/apache2/
#	|-- apache2.conf
#	|	|--  ports.conf
#	|-- mods-enabled
#	|	|-- *.load
#	|	|-- *.conf
#	|-- conf-enabled
#	|	|-- *.conf
#	+-- sites-enabled
#		|-- *.conf
#
#
# * apache2.conf is the main configuration file (this file). It puts the pieces
#   together by including all remaining configuration files when starting up the
#   web server.
#
# * ports.conf is always included from the main configuration file. It is
#   supposed to determine listening ports for incoming connections which can be
#   customized anytime.
#
# * Configuration files in the mods-enabled/, conf-enabled/ and sites-enabled/
#   directories contain particular configuration snippets which manage modules,
#   global configuration fragments, or virtual host configurations,
#   respectively.
#
#   They are activated by symlinking available configuration files from their
#   respective *-available/ counterparts. These should be managed by using our
#   helpers a2enmod/a2dismod, a2ensite/a2dissite and a2enconf/a2disconf. See
#   their respective man pages for detailed information.
#
# * The binary is called apache2. Due to the use of environment variables, in
#   the default configuration, apache2 needs to be started/stopped with
#   /etc/init.d/apache2 or apache2ctl. Calling /usr/bin/apache2 directly will not
#   work with the default configuration.

#
# Global configuration
#

ServerName ${_APACHE_SERVER_NAME_}

#
# ServerRoot: The top of the directory tree under which the server's
# configuration, error, and log files are kept.
#
# NOTE!  If you intend to place this on an NFS (or otherwise network)
# mounted filesystem then please read the Mutex documentation (available
# at <URL:http://httpd.apache.org/docs/2.4/mod/core.html#mutex>);
# you will save yourself a lot of trouble.
#
# Do NOT add a slash at the end of the directory path.
#
#ServerRoot \"/etc/apache2\"

#
# The accept serialization lock file MUST BE STORED ON A LOCAL DISK.
#
#Mutex file:\${APACHE_LOCK_DIR} default

#
# The directory where shm and other runtime files will be stored.
#

DefaultRuntimeDir \${APACHE_RUN_DIR}

#
# PidFile: The file in which the server should record its process
# identification number when it starts.
# This needs to be set in /etc/apache2/envvars
#
PidFile \${APACHE_PID_FILE}

#
# Timeout: The number of seconds before receives and sends time out.
#
Timeout 300

#
# KeepAlive: Whether or not to allow persistent connections (more than
# one request per connection). Set to \"Off\" to deactivate.
#
KeepAlive On

#
# MaxKeepAliveRequests: The maximum number of requests to allow
# during a persistent connection. Set to 0 to allow an unlimited amount.
# We recommend you leave this number high, for maximum performance.
#
MaxKeepAliveRequests 100

#
# KeepAliveTimeout: Number of seconds to wait for the next request from the
# same client on the same connection.
#
KeepAliveTimeout 5


# These need to be set in /etc/apache2/envvars
User \${APACHE_RUN_USER}
Group \${APACHE_RUN_GROUP}

#
# HostnameLookups: Log the names of clients or just their IP addresses
# e.g., www.apache.org (on) or 204.62.129.132 (off).
# The default is off because it'd be overall better for the net if people
# had to knowingly turn this feature on, since enabling it means that
# each client request will result in AT LEAST one lookup request to the
# nameserver.
#
HostnameLookups Off

# ErrorLog: The location of the error log file.
# If you do not specify an ErrorLog directive within a <VirtualHost>
# container, error messages relating to that virtual host will be
# logged here.  If you *do* define an error logfile for a <VirtualHost>
# container, that host's errors will be logged there and not here.
#
ErrorLog \${APACHE_LOG_DIR}/error.log

#
# LogLevel: Control the severity of messages logged to the error_log.
# Available values: trace8, ..., trace1, debug, info, notice, warn,
# error, crit, alert, emerg.
# It is also possible to configure the log level for particular modules, e.g.
# \"LogLevel info ssl:warn\"
#
LogLevel trace1

# Include module configuration:
IncludeOptional mods-enabled/*.load
IncludeOptional mods-enabled/*.conf

# Include list of ports to listen on
Include ports.conf


# Sets the default security model of the Apache2 HTTPD server. It does
# not allow access to the root filesystem outside of /usr/share and /var/www.
# The former is used by web applications packaged in Debian,
# the latter may be used for local directories served by the web server. If
# your system is serving content from a sub-directory in /srv you must allow
# access here, or in any related virtual host.
<Directory />
	Options FollowSymLinks
	AllowOverride None
	Require all denied
</Directory>

<Directory /usr/share>
	AllowOverride None
	Require all granted
</Directory>

<Directory /var/www/>
	Options Indexes FollowSymLinks
	AllowOverride None
	Require all granted
</Directory>

#<Directory /srv/>
#	Options Indexes FollowSymLinks
#	AllowOverride None
#	Require all granted
#</Directory>

# AccessFileName: The name of the file to look for in each directory
# for additional configuration directives.  See also the AllowOverride
# directive.
#
AccessFileName .htaccess

#
# The following lines prevent .htaccess and .htpasswd files from being
# viewed by Web clients.
#
<FilesMatch \"^\.ht\">
	Require all denied
</FilesMatch>

#
# The following directives define some format nicknames for use with
# a CustomLog directive.
#
# These deviate from the Common Log Format definitions in that they use %O
# (the actual bytes sent including headers) instead of %b (the size of the
# requested file), because the latter makes it impossible to detect partial
# requests.
#
# Note that the use of %{X-Forwarded-For}i instead of %h is not recommended.
# Use mod_remoteip instead.
#
LogFormat \"%v:%p %h %l %u %t \\\"%r\\\" %>s %O \\\"%{Referer}i\\\" \\\"%{User-Agent}i\\\"\" vhost_combined
LogFormat \"%h %l %u %t \\\"%r\\\" %>s %O \\\"%{Referer}i\\\" \\\"%{User-Agent}i\\\"\" combined
LogFormat \"%h %l %u %t \\\"%r\\\" %>s %O\" common
LogFormat \"%{Referer}i -> %U\" referer
LogFormat \"%{User-agent}i\" agent

# Include of directories ignores editors' and dpkg's backup files,
# see README.Debian for details.

# Include generic snippets of statements
IncludeOptional conf-enabled/*.conf

# Include the virtual host configurations:
IncludeOptional sites-enabled/*.conf

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet"

if [ "${mod_mode}" = "${release}" ]; then
	declare -rg dockerfile_debug_tools_t=""
else
	declare -rg dockerfile_debug_tools_t="\
# debug only
RUN apt-get -y install nmap
RUN apt-get -y install net-tools"
fi

declare -rg dockerfile_t="\
# Ref https://hub.docker.com/_/php/
# php:7.3-apache-stretch
# Apache/2.4.25 (Debian)
# PHP 7.3 Cli
ARG VERSION=7.3.9-apache-stretch
FROM php:\$VERSION AS base

RUN apt-get update
RUN apt-get -y install sudo

${dockerfile_debug_tools_t}

# install apxs tools
RUN apt-get -y install apache2-dev

# mysqli driver
RUN docker-php-ext-install mysqli

#  mysql dbd
#RUN apt-get -y install libaprutil1-dbd-mysql

# serivce and a2* need root access
USER root

# import mod_authnz_external source
COPY ${_APACHE_MOD_AUTHNZ_EXTERNAL_D_} /${_APACHE_MOD_AUTHNZ_EXTERNAL_D_}

# build and install mod_authnz_external
WORKDIR /${_APACHE_MOD_AUTHNZ_EXTERNAL_D_}
RUN	apxs -c mod_authnz_external.c
RUN	sudo apxs -i -a mod_authnz_external.la

# stop apache, it starts with docker-compose
RUN [\"/bin/bash\", \"-c\", \"service apache2 stop\"]

# disable default conf
RUN [\"/bin/bash\", \"-c\", \"a2dissite 000-default\"]

# enable proxy
RUN [\"/bin/bash\", \"-c\", \"a2enmod proxy\"]
RUN [\"/bin/bash\", \"-c\", \"a2enmod proxy_http\"]
RUN [\"/bin/bash\", \"-c\", \"a2enmod proxy_ajp\"]
RUN [\"/bin/bash\", \"-c\", \"a2enmod rewrite\"]
RUN [\"/bin/bash\", \"-c\", \"a2enmod deflate\"]
RUN [\"/bin/bash\", \"-c\", \"a2enmod headers\"]
RUN [\"/bin/bash\", \"-c\", \"a2enmod proxy_balancer\"]
RUN [\"/bin/bash\", \"-c\", \"a2enmod proxy_connect\"]
RUN [\"/bin/bash\", \"-c\", \"a2enmod proxy_html\"]

# enable mod_authnz_external
RUN [\"/bin/bash\", \"-c\", \"a2enmod authnz_external\"]

# enable dbd
#RUN [\"/bin/bash\", \"-c\", \"a2enmod dbd\"]
#RUN [\"/bin/bash\", \"-c\", \"a2enmod authn_dbd\"]
#RUN [\"/bin/bash\", \"-c\", \"a2enmod authn_socache\"]

# enable session
#RUN [\"/bin/bash\", \"-c\", \"a2enmod session\"]
#RUN [\"/bin/bash\", \"-c\", \"a2enmod session_dbd\"]

# enable auth form
#RUN [\"/bin/bash\", \"-c\", \"a2enmod request\"]
#RUN [\"/bin/bash\", \"-c\", \"a2enmod auth_form\"]

# enable SSL
RUN [\"/bin/bash\", \"-c\", \"a2enmod ssl\"]"

declare -r http_authentication_bang_t="\
#!/bin/bash

for key in username password; do
	read -r value; eval \"declare -r \$key=\\\"\$value\\\"\"
done

declare -ir http_expected_status=\"${_APACHE_MOD_AUTHNZ_SUCCESS_CODE_}\"
declare -r php_api=\"https://${_APACHE_MOD_AUTHNZ_LOCALHOST_}/login/auth\"

declare curl_flags
curl_flags+=\" -k -L --post301 --post302 --post303\"
curl_flags+=\" -w %{http_code}\"
curl_flags+=\" -o ${_APACHE_MOD_AUTHNZ_DEBUG_FLAG_}\"
curl_flags+=\" -H \\\"${_APACHE_MOD_AUTHNZ_H_ACCEPT_}\\\"\"
curl_flags+=\" -H \\\"${_APACHE_MOD_AUTHNZ_H_CONTENT_}\\\"\"
curl_flags+=\" --data \\\"username=\$username&password=\$password\\\"\"

declare -ir http_status=\"\$(eval \"curl \$curl_flags \$php_api\")\"
[ \"\$http_status\" -eq \"\$http_expected_status\" ]
declare -ir _CODE=\"\$?\"

echo \"\$(date +'%d/%m/%Y - %H:%M:%S'): \${BASH_SOURCE[0]}: HTTP_STATUS: \$http_status\" >&2
exit \$_CODE"
