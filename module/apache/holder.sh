#!/bin/bash
#
# apache placeholders
#
# Copyright 2019 Luigi Santivetti <luigi.santivetti@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

_APACHE_DKRC_SERVICE_="${module}"
_APACHE_DKRC_IMAGE_="${host_name}-img-${_APACHE_DKRC_SERVICE_}"
_APACHE_DKRC_CONTEXT_="${mod_docker_d}"
_APACHE_DKRC_CONTAINER_="${host_name}-con-${_APACHE_DKRC_SERVICE_}"
_APACHE_DKRC_FRONTEND_IP_="172.28.0.2"
_APACHE_DKRC_BACKEND_IP_="172.27.0.3"
_APACHE_DKRC_PORTS_F_="${ports_f}"
_APACHE_DKRC_CONF_F_="${apache2_f}"
_APACHE_DKRC_AVAILABLE_D_="${available_d}"
_APACHE_DKRC_ENABLED_D_="${enabled_d}"
_APACHE_DKRC_CERT_L_="${certificates_l}"
_APACHE_DKRC_LOG_D_="${log_d}"
_APACHE_DKRC_WWW_D_="${www_d}"
if [ "$apache_has_media" -eq 1 ]; then
	_APACHE_HAS_MEDIA_="1"
	_APACHE_HTML_PHOTOS_D_="${photos_d}"
	_APACHE_HTML_VIDEOS_D_="${videos_d}"
else
	_APACHE_HAS_MEDIA_="0"
	_APACHE_HTML_PHOTOS_D_=""
	_APACHE_HTML_VIDEOS_D_=""
fi
_APACHE_DKRC_ROOTFS_="${mod_rootfs_d}"
_APACHE_DKRC_DOCKERFILE_="Dockerfile"
_APACHE_VHOST_HTTP_LOG_F_="/var/log/apache2/vhost-http-log.txt"
_APACHE_SSL_LOG_F_="/var/log/apache2/vhost-https-log.txt"
_APACHE_SSL_LOG_LEVEL_REL_="info ssl:info"
_APACHE_SSL_LOG_LEVEL_DBG_="trace8 ssl:trace8"
_APACHE_CON_SSL_D_="/etc/letsencrypt"
if [ "$apache_has_dummy_certs" -eq 1 ]; then
	_APACHE_CON_SSL_CONF_F_="${_APACHE_CON_SSL_D_}/${host_name}.conf"
	_APACHE_CON_SSL_CERT_F_="${_APACHE_CON_SSL_D_}/${host_name}.crt"
	_APACHE_CON_SSL_KEY_F_="${_APACHE_CON_SSL_D_}/${host_name}.key"
else
	_APACHE_CON_SSL_CONF_F_="${_APACHE_CON_SSL_D_}/options-ssl-apache.conf"
	_APACHE_CON_SSL_CERT_F_="${_APACHE_CON_SSL_D_}/live/${host_name}/fullchain.pem"
	_APACHE_CON_SSL_KEY_F_="${_APACHE_CON_SSL_D_}/live/${host_name}/privkey.pem"
fi
_APACHE_VHOST_GERRIT_LOG_F_="/var/log/apache2/vhost-gerrit-log.txt"
_APACHE_VHOST_GERRIT_LOG_LEVEL_REL_="info"
_APACHE_VHOST_GERRIT_LOG_LEVEL_DBG_="trace8"
_APACHE_DOCUMENT_ROOT_D_="/var/www/html"
_APACHE_PRIVATE_PATH_="/private"
_APACHE_HTTP_PORT_="80"
_APACHE_HTTPS_PORT_="443"
_APACHE_GERRIT_PROXY_PORT_="8081"
_APACHE_SERVER_NAME_="${host_name}"
_APACHE_SERVER_ALIAS_="www.${_APACHE_SERVER_NAME_}"
_APACHE_CON_SSL_MYSQL_D_="/etc/mysql_ssl"
_APACHE_CON_SSL_MYSQL_CERT_F_="${_APACHE_CON_SSL_MYSQL_D_}/"
_APACHE_CON_SSL_MYSQL_CERT_F_+="$(basename "${_MYSQL_SSL_CLIENT_CERT_F_}")"
_APACHE_CON_SSL_MYSQL_KEY_F_="${_APACHE_CON_SSL_MYSQL_D_}/"
_APACHE_CON_SSL_MYSQL_KEY_F_+="$(basename "${_MYSQL_SSL_CLIENT_KEY_F_}")"
_APACHE_CON_SSL_MYSQL_CA_F_="${_APACHE_CON_SSL_MYSQL_D_}/"
_APACHE_CON_SSL_MYSQL_CA_F_+="$(basename "${_MYSQL_SSL_CLIENT_CA_F_}")"
_APACHE_EXT_AUTH_F_="${http_authentication_bang_f}"
_APACHE_EXT_AUTH_KEYWORD_="bash_auth"
_APACHE_CON_EXT_AUTH_F_="/var/www/http_authentication"
if [ "${mod_mode}" = "${release}" ]; then
	_APACHE_SSL_LOG_LEVEL_="${_APACHE_SSL_LOG_LEVEL_REL_}"
	_APACHE_VHOST_GERRIT_LOG_LEVEL_="${_APACHE_VHOST_GERRIT_LOG_LEVEL_REL_}"
	_APACHE_MOD_AUTHNZ_DEBUG_FLAG_="/dev/null"
	_APACHE_CODE_IGNITER_ENV_="production"
else
	_APACHE_SSL_LOG_LEVEL_="${_APACHE_SSL_LOG_LEVEL_DBG_}"
	_APACHE_VHOST_GERRIT_LOG_LEVEL_="${_APACHE_VHOST_GERRIT_LOG_LEVEL_DBG_}"
	_APACHE_MOD_AUTHNZ_DEBUG_FLAG_="/dev/stderr --verbose"
	_APACHE_CODE_IGNITER_ENV_="development"
fi
_APACHE_MOD_AUTHNZ_EXTERNAL_D_="$(basename "${mod_authnz_external_d}")"
_APACHE_MOD_AUTHNZ_SUCCESS_CODE_="204"
_APACHE_MOD_AUTHNZ_H_ACCEPT_="Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
_APACHE_MOD_AUTHNZ_H_CONTENT_="Content-Type: application/x-www-form-urlencoded"
_APACHE_MOD_AUTHNZ_LOCALHOST_="127.0.0.1"
