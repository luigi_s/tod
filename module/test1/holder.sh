#!/bin/bash
#
# holder.sh - test
#
# Copyright 2019 Luigi Santivetti <luigi.santivetti@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Test plain variable
_TEST1_VAR0_HOLDER_="test1_var0_holder"

# Test variable externally defined within test1 module
_TEST1_VAR1_MODULE_="$mod_name_clash"

# Test variable externally defined in foreign holder and composite
_TEST1_VAR2_EXTREF_="${_TEST2_VAR0_HOLDER_}-notnull"

# Test variable externally defined in foreign module and composite
_TEST1_VAR3_EXTREF_="${_TEST2_VAR1_MODULE_}-notnull"

# Test circular dependencies. Uncomment will cause validation to detect a loop
#_TEST1_VAR4_EXTREF_="${_TEST2_VAR4_HOLDER_}"
#_TEST1_VAR5_EXTREF_="${_TEST2_VAR5_MODULE_}"

# Test variable externally defined in foreign holder
_TEST1_VAR6_EXTREF_="${_TEST2_VAR0_HOLDER_}"

# Test variable externally defined in foreign module
_TEST1_VAR7_EXTREF_="${_TEST2_VAR1_MODULE_}"

# Test command substitution with unescaped quote symbols
_TEST1_VAR8_CMDSUB_="text $(dirname "$(basename "$(dirname "$(pwd)")")")"

# Test LHS validation. Uncomment will cause validation to detect invalid LHS
#_NOVALID_VAR9_LHS_="invalid LHS. First token must match module's name"

# Test RHS validation. Uncomment will cause validation to detect multiple
# commands in line.
#_TEST1_VAR10_MULTIPLE_CMD_="legal RHS"; echo "Illegal continuation"
#_TEST1_VAR11_MULTIPLE_CMD_="legal RHS" && _TEST1_VAR12_="illegal continuation" 
#_TEST1_VAR13_MULTIPLE_CMD_="legal RHS"; if :;then echo "true"; fi 

# Test if/else
if true; then
	_TEST1_VAR14_="true"
else
	_TEST1_VAR15_="false"
fi

# Test incremental assignment
_TEST1_VAR16_="var15"
_TEST1_VAR16_+=" + var15"

# FIXME: test for null value. Validation does not pick this up if in the same
# module. holder.sh will never trigger an error cause RHS isn't checked for
# local holder variables.
_TEST1_VAR17_=""

# Test for reference null value. Uncomment will cause validation to detect a null
# value for a referenced variable.
#_TEST1_VAR17_="${_TEST2_VAR6_EXTREF_}"

# Test for reference unbound. Uncomment will cause validation to detect an unbound
# variable.
#_TEST1_VAR18_="${_TEST2_VAR6_EXTREF_}"

# Test for unknown variable name. Uncomment will cause validation to detect an
# unknown variable name on the RHS.
#_TEST1_VAR19_="${this_isn_t_a_valid_reference_name}"

# Test for invalid module name. Uncomment will cause validation to detect an
# invalid module name.
#_TEST1_VAR20_="${_MODULEFOO_DOES_NOT_EXISTS_}"
