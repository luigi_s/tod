#!/bin/bash
#
# holder.sh - test
#
# Copyright 2019 Luigi Santivetti <luigi.santivetti@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

declare -r test_t="\

test1 - scheme.sh - test_t

@@ START >>>>>>>>>>>>>
${_TEST1_VAR0_HOLDER_}
${_TEST1_VAR1_MODULE_}
${_TEST1_VAR2_EXTREF_}
${_TEST1_VAR3_EXTREF_}
\${_TEST1_VAR4_EXTREF_}
\${_TEST1_VAR5_EXTREF_}
${_TEST1_VAR6_EXTREF_}
${_TEST1_VAR7_EXTREF_}
\${_TEST3_VAR7_UNBOUND_}
@@ END <<<<<<<<<<<<<<<
"
