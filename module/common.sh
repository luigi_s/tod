#!/bin/bash
#
# common - module.sh
#
# Copyright 2019 Luigi Santivetti <luigi.santivetti@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

declare -r mod_module_d="$module_d/$module"
declare -r mod_instance_d="$instance_d"
declare -r mod_docker_d="$docker_d/$module"
declare -r mod_rootfs_d="$rootfs_d"
declare -r mod_mode_env="${module^^}_MODE"
declare -r mod_mode="$(eval "echo \${$mod_mode_env:-$instance_mode}")"
declare -r mod_staging_d="$mod_module_d/$staging"

declare -ar mod_dirs=( $mod_instance_d $mod_docker_d $mod_rootfs_d )
declare -ar mod_files=( )
declare -ar mod_trefs=( )

if [ "${mod_mode}" != "${debug}" ] && [ "${mod_mode}" != "${release}" ]; then
	exit 1
fi

# @1      : module name
# @2      : absolute path to module staging dir
function __fetch_module_common
{
	local modname
	local tmp_d
	local tar

	[ -z "$1" ] && {
		[ -n "$module" ] || {
			lets -l -e "invalid module"; return $s_err
		}; modname="$module"
	} || modname=$1

	[ -z "$2" ] && {
		[ -n "$mod_staging_d" ] && [ -d "$mod_module_d" ] || {
			lets -l -e "invalid module root"; return $s_err
		}; tmp_d="$mod_staging_d"
	} || tmp_d=$2

	is_unix_path $tmp_d || { lets -l -e "invalid staging area"; return $s_err; }

	[ -d "$tmp_d" ] && {
		lets --ask "${tmp_d##$module_d/} existing, wipe it?" && rm -rf $tmp_d || \
				lets -l -w "keeping staging"
	}

	mkdir -p $tmp_d && process_manifest $modname $tmp_d || {

			# Wipe the whole staging_d for this module
			[ "$tmp_d" = "$mod_staging_d" ] && rm -rf $tmp_d
			return $s_err
		}
}

function __dir_do_common_helper
{
	local -a __a1=( )

	if [ -z "${mod_more_dirs[*]}" ] || [ ${#mod_more_dirs[@]} -eq 0 ]; then
		lets -l -w "$module: does not have more dirs"
		__a1=( "${mod_dirs[@]}" )
	else
		__a1=( $(__merge_array "${mod_dirs[@]}" "${mod_more_dirs[@]}") )
	fi

	create_directory "${__a1[@]}" || return $s_err
}

function __file_do_common_helper
{
	local -a __a1=( ); local -a __a2=( )

	if [ -z "${mod_more_files[*]}" ] || [ ${#mod_more_files[@]} -eq 0 ]; then
		lets -l -w "$module: does not have more files"
	else
		__a1=( $(__merge_array "${mod_files[@]}" "${mod_more_files[@]}") )
	fi

	if [ -z "${mod_more_trefs[*]}" ] || [ ${#mod_more_trefs[@]} -eq 0 ]; then
		lets -l -w "$module: does not have more templates"
	else
		__a2=( $(__merge_array "${mod_trefs[@]}" "${mod_more_trefs[@]}") )
	fi

	__file_do "$1" "$uninstall_f" "${__a1[@]}" "${__a2[@]}" || return $s_err
}

function __doins_module_common
{
	__dir_do_common_helper && __file_do_common_helper --create
}

function __watch_module_common
{
	__file_do_common_helper --splash
}

function __upins_module_common
{
	__dir_do_common_helper && __file_do_common_helper --update
}

function __clmod_module_common
{
	rm -rf $mod_staging_d || sudo rm -rf $mod_staging_d
}

function __clins_module_common
{
	local -a __a1=( ); local -a __a2=( )

	__file_do --update \
		"$uninstall_f" "${__a1[@]}" "${__a2[@]}" || return $s_err
}
