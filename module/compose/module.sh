#!/bin/bash
#
# compose - module.sh
#
# Copyright 2019 Luigi Santivetti <luigi.santivetti@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

declare -r module="compose"
source $common_sh

module_enable $module
declare -ar depmod=( gerrit mysql apache )

declare -r environment="common.env"
declare -r environment_f="${instance_d}/${environment}"
declare -r docker_compose_f="${instance_d}/docker-compose.yml"
declare -r compose_cli_f="${instance_d}/compose-cli.sh"

declare -ar mod_more_files=( $docker_compose_f $environment_f $compose_cli_f )
declare -ar mod_more_trefs=( docker_compose_t environment_t compose_cli_bang_t )

function tod_watch
{
	__watch_module_common || return $s_err
}

function tod_doall
{
	__doins_module_common || return $s_err
}

function tod_upins
{
	__upins_module_common || return $s_err
}

function tod_clins
{
	__clins_module_common || return $s_err
}
