#!/bin/bash
#
# compose - holder.sh
#
# Copyright 2019 Luigi Santivetti <luigi.santivetti@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

_COMPOSE_API_VERSION_="3"
_COMPOSE_HOST_HTTP_PORT_="80"
_COMPOSE_HOST_HTTPS_PORT_="443"
_COMPOSE_HOST_SSH_PORT_="29418"
_COMPOSE_SUBNET_BIT_="16"
_COMPOSE_SUBNET_FRONTEND_IP_="172.28.0.0"
_COMPOSE_SUBNET_BACKEND_IP_="172.27.0.0"
_COMPOSE_ENVIRONMENT_="${environment_f}"
