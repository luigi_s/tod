#!/bin/bash
#
# help.sh
#
# Copyright 2019 Luigi Santivetti <luigi.santivetti@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

function __help
{
	cat <<EOF | less -RF
`printf "\033[1m%s\033[0m\n" "NAME"`

  tod - template open deploy

`printf "\033[1m%s\033[0m\n" "USAGE"`

  [ENVIRONMENT] tod [OPTION]=[MODULE,...]

`printf "\033[1m%s\033[0m\n" "OPTIONS"`

  --check        Validate module: module.sh, holder.sh and scheme.sh
  --watch        Diff installed files against templates in scheme.sh
  --fetch        Download MANIFEST mirrors into \$mod_staging_d
  --doall        Make and install module files and \$instance_d files
  --doins        Make and install \$instance_d files, if applicable
                 ignore module files
  --upall        Update module files and \$instance_d files
  --upins        Update \$instance_d files, if applicable ignore module
                 files
  --upmod        Update module files only, do not touch \$instance_d files
  --clins        Uninstall \$instance_d files tracked in uninstall.sh
  --clmod        Remove \$mod_staging_d

`printf "\033[1m%s\033[0m\n" "MODULES"`

`for m in $(get_all_modules); do echo "  $m"; done`

`printf "\033[1m%s\033[0m\n" "ENVIRONMENT"`

  LOG_D          Default $LOG_D, 1 to show debug logs
  LOG_W          Default $LOG_W, 0 to suppress warning logs
  LOG_I          Default $LOG_I, 0 to suppress info logs
  LOG_X          Default $LOG_X, 0 to suppress logs from sub-process pipes
  INSTANCE_DIR   Default to \$script_dir, which defaults to the tod dir, set it
                 with the path to where an instance is to be created
  HOST_NAME      Default "$def_hostname", instance name
  PASSWD_F       Default null. Only needed for docker-compose and compose-cli.sh.
                 Path to a file containing runtime passwords. This file can be
                 removed after an instance is started
  DEFANS         Default null. Possible values are 'y' or 'n', allows to skip
                 user input when asked
  MODALL         Default 0, 1 to force the use of blacklisted modules. To
                 blacklist a module add it to BLMODULES in config. Blacklisted
                 modules are excluded by the list of modules run by default

  blacklisted modules:

`for m in ${BLMODULES[@]}; do echo "  $m"; done`

  INSTANCE_MODE  Default "$debug", or "$release". Global switch to enable
                 debug or release version of an instance. Each module is
                 responsible for distinguishing between them. This option
                 can be overridden by module specific MODE. See example [4]

  module specific MODE overrides INSTANCE_MODE:

`for m in $(get_all_modules); do \\
printf "  %-14s %s\n" "${m^^}_MODE" \\
"Default \\\$INSTANCE_MODE, \"$debug\" or \"$release\"";\\
 done`

`printf "\033[1m%s\033[0m\n" "EXAMPLES"`

  [1] Call doall on every module, exclude blacklisted modules
  ./tod --doall

  [2] Force blacklist modules, call doall on test1 only
  MODALL=1 ./tod --doall=test1

  [3] Force blacklist modules, call doall on test1 and test2
  MODALL=1 ./tod --doall=test1,test2

  [4] Enable debug logs, make a release instance, but test2 debug
  LOG_D=1 INSTANCE_MODE=release TEST2_MODE=debug ./tod --doall=test1,test2

`printf "\033[1m%s\033[0m\n" "AUTHORS"`

  Luigi Santivetti

`printf "\033[1m%s\033[0m\n" "REPORTING BUGS"`

  luigi.santivetti@gmail.com
  https://bitbucket.org/luigi_s/tod

`printf "\033[1m%s\033[0m\n" "VERSION"`

  $credits_vers

`printf "\033[1m%s\033[0m\n" "COPYRIGHT"`

  Copyright 2020

`printf "\033[1m%s\033[0m\n" "END"`
EOF
}
